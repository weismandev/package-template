import { describe, expect, test } from 'vitest';

import { sum } from '@/src';

// The two tests marked with concurrent will be run in parallel
describe('suite', () => {
  test('default', () => {
    expect(sum(5, 5)).toBe(10);
  });
});

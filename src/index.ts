import { createEvent, createStore, sample } from 'effector';

export const sum = (a: number, b: number): number => a + b;

const $store = createStore(null);
const event = createEvent();

sample({
  clock: event,
  source: $store,
  target: $store,
});
